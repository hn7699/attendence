package fi.vamk.e1800957.contine_attendance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class AttendanceController {
    @Autowired
    private AttendanceRepository repository;

    @GetMapping("/attendances")
    public Iterable<AttendanceModel> list() {
    	ArrayList< AttendanceModel> list= new ArrayList<AttendanceModel>();
    	for (Attendance a : repository.findAll()) {
			list.add(a.convert());
		}
        return list;
    }
    
    
    //Adding new methods get by date
    
    @GetMapping("/attendances/{date}")
    public AttendanceModel getByDate(@PathVariable("date") String date) {
		try {			
			Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
			return repository.findByDay(date1).convert();
		} catch (ParseException e) {
			return null;
		}
		
    }

    @PostMapping("/attendance")
    public @ResponseBody AttendanceModel create(@RequestBody AttendanceModel item) {
    	Attendance a= item.convert();
    	if (a.getDay() == null) return null;
        return repository.save(a).convert();
    }

    @PutMapping("/attendance")
    public @ResponseBody AttendanceModel update(@RequestBody AttendanceModel item) {
    	Attendance a= item.convert();
    	if (a.getDay() == null) return null;
        return repository.save(a).convert();
    }

    @DeleteMapping("/attendance")
    public void date(@RequestBody AttendanceModel item) {
    	Attendance a= item.convert();
    	repository.delete(a);
    }
}