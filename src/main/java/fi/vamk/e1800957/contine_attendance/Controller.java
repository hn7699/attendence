package fi.vamk.e1800957.contine_attendance;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class Controller {
    @RequestMapping("/test")
    public String test() {
        return "{\"id\":1}";
    }
}