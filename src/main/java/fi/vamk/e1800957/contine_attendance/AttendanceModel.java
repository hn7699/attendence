package fi.vamk.e1800957.contine_attendance;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class AttendanceModel {
	
	private int id;
	private String key;
	private String day;
	
	public AttendanceModel(int id, String key, String day) {
		super();
		this.id = id;
		this.key = key;
		this.day = day;
	}
	public String getDay() {
		return this.day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	public Attendance convert()
	{
		try {
			return new Attendance( this.getId(), this.getKey(), new SimpleDateFormat("dd-MM-yyyy").parse(this.getDay()));
		} catch (ParseException e) {
			return new Attendance( this.getId(), this.getKey(), null);
		}		
	}
	public String toString() {
		return id + "-" + key + "-" + day;
	}

}
